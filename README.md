
# eClient
[![justforfunnoreally.dev badge](https://img.shields.io/badge/justforfunnoreally-dev-9ff)](https://justforfunnoreally.dev)
![image](https://github.com/v-pun215/eClient/assets/67716965/d73139fc-5331-49b4-a0a8-a600c6c67cdf)


A Minecraft Launcher written in Python.

Download Installer: https://github.com/v-pun215/eClient/releases/latest/download/install.exe

Based on PyCraft by shasankp000.

See his work: https://github.com/shasankp000/PyCraft
## [eClient Lite](https://github.com/v-pun215/eClient-Lite/) Released!
## Important: About the installer.
The install.exe downloads all libraries from https://libs-pi.vercel.app/eclient.zip.

All install.exe do this (even setup.exe) 

So, if you want a older version of eClient, download source code from releases.


## Logo:
![icon](https://github.com/v-pun215/eClient/assets/67716965/ca4560eb-dc12-41ef-9b45-0ede2d32ffca)


## Project Overview
This launcher aims to be the best launcher (that I can code) built for both Windows (tested) and Linux (not tested).

Update for macOS is coming soon.

This launcher downloads Minecraft Libraries from the official libraries.minecraft.net.

This launcher's main goal is not to pirate the game, it's simply to port the launcher to Python :)
## Known Bugs -
When launching Minecraft 1.20, Minecraft loads in with 'Failed to Quick Play'. Trying to fix this issue.

Not able to login to Mojang accounts anymore (Mojang disabled the API).

Forge can't install and run properly.
## Features
Can install and launch all versions of Minecraft Java Edition (starting from 1.0)

Can install mods directly from the launcher

Supports Fabric.

Ability to login with M̶o̶j̶a̶n̶g̶, Ely.by and Offline Accounts.

Never gives you up.
## Screenshots
eClient Updater
![image](https://github.com/v-pun215/eClient/assets/67716965/1832df19-6d37-40fe-a5d5-140efd597e55)




eClient (Home)
![mc1](https://github.com/v-pun215/eClient/assets/67716965/419a3ab7-707a-4128-85c0-d5109310fd90)


eClient (Installations tab)
![mc2](https://github.com/v-pun215/eClient/assets/67716965/b4d67b51-2d98-4a8c-b163-4fbffcccaf4c)


eClient (Settings tab)
![mc3](https://github.com/v-pun215/eClient/assets/67716965/87d7c3c5-49d6-4faa-a02f-616928aa070b)


eClient (Additional Settings tab)
![mc4](https://github.com/v-pun215/eClient/assets/67716965/70d93c6e-4e0c-4b1d-a318-3969f038ea99)


eClient (Help tab)
![image](https://github.com/v-pun215/eClient/assets/67716965/fdc1d5fa-1510-441a-802a-bff86b3f53e2)


eClient (Mod Installer)
![mc6](https://github.com/v-pun215/eClient/assets/67716965/6d147b3c-e1c5-4aca-85b0-ba73a3e35cc7)

eClient (Accounts Window)
![mc7](https://github.com/v-pun215/eClient/assets/67716965/9acf0115-33fa-402e-8fbe-3bfcce503705)

## Installation
Check releases tab.
    
